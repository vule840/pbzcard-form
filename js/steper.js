$(document).ready(function () {
    


    //PRIKAZI/SAKRIJ HEADER
    var tab1 = $("a[href='#fizicka-osoba']")
    var tab2 = $("a[href='#poslovni-subjekt']")
    var headerSection = $(".headerSection .textHolder h1")
    var headerSectionBg = $(".headerSection")
    var fizTx = "Odgoda plaćanja obveza po kreditnim karticama za fizičke osobe";
    var posTx = "Odgoda plaćanja obveza po kreditnim karticama za poslovne subjekte"

    $(tab1).click(function () {

        console.log("tab1");
        headerSection.animate({ opacity: 1 }, 100).html(fizTx)

        headerSectionBg.css({ 'background-image': 'url("img/fizicka-osoba.jpg")' }).animate({ opacity: 1 }, { duration: 30 });
    });
    $(tab2).click(function () {

        console.log("tab2");

        headerSection.html(posTx).animate({ opacity: 1 }, 100);
        headerSectionBg.css({ 'background-image': 'url("img/poslovna-osoba.jpg")' }).animate({ opacity: 1 }, { duration: 30 });
    });

   



        // OIB KOMBINACIJA 
        //FIZICKE 
        $("#brojkartice").on("keyup keypress blur change",function(){
            var visa = $(".kartice")
            var oibKartice = $("#brojkartice");
            console.log(oibKartice);
    
            if (oibKartice[0].value == "1234" || oibKartice[0].value == "1111") {
                
                visa.css("display","flex")
                visa.fadeIn(300)
    
            } else{
                visa.fadeOut(300)
            }
        })
        //POSLOVNE 
        $("#brojkarticePoslovni").on("keyup keypress blur change",function(){
            var visa = $(".kartice")
            var oibKartice = $("#brojkarticePoslovni");
            console.log(oibKartice);
    
            if (oibKartice[0].value == "1234" || oibKartice[0].value == "1111") {
                
                visa.css("display","flex")
                visa.fadeIn(300)
    
            } else{
                visa.fadeOut(300)
            }
        })

    //OSTALO - KORAK 3/DOKUMENTI - KORAK 4 JE POJAVI
    $("input[type='radio']").click(function () {
        var choice4 = $("input#choice4")
        var choice20 = $("input#choice20")
        var choice5 = $("input#choice5")
        var choice6 = $("input#choice6")
        var choice21 = $("input#choice21")
        var choice22 = $("input#choice22")
        var radiotextarea = $(".radio-textarea")
        var doc1 = $(".dokumenti")
        var doc2 = $(".dokumenti2")
        var doc3 = $(".dokumenti3")
        var doc4 = $(".dokumenti4")
        //radio klik, pojavi ostalo
        if (choice4.is(":checked") || choice20.is(":checked")) {

            radiotextarea.fadeIn(150)
        } else {
            radiotextarea.fadeOut(150)
        }


        //radio klik, pojave dokumenti - Kredit
        if (choice5.is(":checked")) {

            doc1.fadeIn(150)
        } else {
            doc1.fadeOut(150)
        }

        //radio klik, pojave dokumenti - Račun
        if (choice6.is(":checked")) {

            doc2.fadeIn(150)
        } else {
            doc2.fadeOut(150)
        }

        //radio klik, pojave dokumenti - Kredit
        if (choice21.is(":checked")) {

            doc3.fadeIn(150)
        } else {
            doc3.fadeOut(150)
        }

        //radio klik, pojave dokumenti - Račun
        if (choice22.is(":checked")) {

            doc4.fadeIn(150)
        } else {
            doc4.fadeOut(150)
        }
    });
    //1. for - OSTALO - KORAK 3 BROJ ZNAKOVA PRIKAŽI  

    var znakovi = $('#ostalo');
    $("#ostalo").on("keyup keypress blur change", function () {
        // console.log(znakovi[0].textLength);

        var brojZnakova = znakovi[0].textLength;
        $(".znak").html(brojZnakova)

        console.log("formData" + formData)
        console.log("serializeArray" + $('#myForm').serializeArray());
    })
    //2. for OSTALO - KORAK 3 BROJ ZNAKOVA PRIKAŽI  
    var znakovi2 = $('#ostalo2');
    $("#ostalo2").on("keyup keypress blur change", function () {
        // console.log(znakovi[0].textLength);

        var brojZnakova = znakovi2[0].textLength;
        $(".znak2").html(brojZnakova)


    })

    //IME DOKUMENTA 

    $('input[type="file"]').change(function (e) {
        var fileName = e.target.files[0].name;
        // alert('The file "' + fileName +  '" has been selected.');

        $(this).prev().text(fileName);
    
    });



    //VALIDATOR 
    
     $.validator.addMethod('oib', function(value, element, param) {
        return (value != 11111111111) 
     }, 'Vaš OIB nije pronađen u našoj bazi');

     $.validator.addMethod('oibPoslovni', function(value, element, param) {
        return (value != 11111111111) 
     }, 'Vaš OIB nije pronađen u našoj bazi');

    // $.validator.addMethod('month', function(value, element, param) {
    //     return (value != 0) && (value <= 12) && (value == parseInt(value, 10));
    // }, 'Please enter a valid month!');
    // $.validator.addMethod('year', function(value, element, param) {
    //     return (value != 0) && (value >= 1900) && (value == parseInt(value, 10));
    // }, 'Please enter a valid year not less than 1900!');
    // $.validator.addMethod('username', function(value, element, param) {
    //     var nameRegex = /^[a-zA-Z0-9]+$/;
    //     return value.match(nameRegex);
    // }, 'Only a-z, A-Z, 0-9 characters are allowed');



    var val = {
        // Specify validation rules
        rules: {
            /***FIZICKE OSOBE***/
            /*1*/
            oib: {
                oib: true,
                required: true,
                minlength: 11,
                maxlength: 11,
                digits: true
            },
            brojKartice: {
                required: true,
                minlength: 4,
                maxlength: 4,
                digits: true
            },
            kartice: {
                required: true
            },
            /*2*/
            ime: "required",
            prezime: "required",
            email: {
                required: true,
                email: true
            },
            telefon: {
                required: true,
                // digits:true,
                minlength: 9
            },
            /*3*/
            razlogOdgode: {
                required: true
            },
            /*4*/
            predmetOdgode: {
                required: true
            },
            zatrazenaOdgoda: {
                required: true
            },
            zatrazenaOdgoda2: {
                required: true
            },
            suglasnost1: {
                required: true
            },
            suglasnost2: {
                required: true
            },
            /*5*/
            prednjaStrana: {

                required: true
            },
            straznjaStrana: {
                required: true
            },
            dokumentacija: {
                required: true
            },


            /***POSLOVNi SUBJEKT***/
            /*1*/
            oibPoslovni: {
                oibPoslovni: true,
                required: true,
                minlength: 11,
                maxlength: 11,
                digits: true
            },
            brojkarticePoslovni: {
                required: true,
                minlength: 4,
                maxlength: 4,
                digits: true
            },
           
            karticePoslovni: {
                required: true
            },
            /*2*/
            imePoslovni: "required",
            prezimePoslovni: "required",
            oibOvlastenePoslovni: {
                required: true,
                minlength: 11,
                maxlength: 11,
                digits: true
            },
            emailPoslovni: {
                required: true,
                email: true
            },
            mobitelPoslovni: {
                required: true,
                // digits:true,
                minlength: 9
            },
            /*3*/

            razlogOdgodePoslovni: {
                required: true
            },

            /*4*/
            predmetOdgodePoslovni: {
                required: true
            },
            zatrazenaOdgodaPoslovni: {
                required: true
            },
            zatrazenaOdgodaPoslovni2: {
                required: true
            },
            
            /*5*/
            prednjaStranaPoslovni: {
                required: true
            },
            straznjaStranaPoslovni: {
                required: true
            },
            dokumentacijaPoslovni: {
                required: true
            },


           
        },
        // Specify validation error messages
        messages: {
            /**FIZICKA OSOBA */
            oib: {
                required: "Broj OIB-a je potreban",
                minlength: "Molimo unesite minimalno 11 znakova",
                maxlength: "Molimo unesite točno 11 znakova",
                digits: "Samo su brojevi dozvoljeni"
            },
            oibPoslovni: {
                required: "Broj OIB-a je potreban",
                minlength: "Molimo unesite minimalno 11 znakova",
                maxlength: "Molimo unesite točno 11 znakova",
                digits: "Samo su brojevi dozvoljeni"
            },
            oibOvlastenePoslovni: {
                required: "Broj OIB-a je potreban",
                minlength: "Molimo unesite minimalno 11 znakova",
                maxlength: "Molimo unesite točno 11 znakova",
                digits: "Samo su brojevi dozvoljeni"
            },
            telefon: {
                required: "Telefon je potrebno polje",
                digits: "Molimo upište broj telefona",
                minlength: "Molimo unesite više brojeva",
            },
            mobitelPoslovni: {
                required: "Telefon je potrebno polje",
                digits: "Molimo upište broj telefona",
                minlength: "Molimo unesite više brojeva",
            },
            kartice:{
                required: "Izaberite vašu karticu",    
            },
            karticePoslovni:{
                required: "Izaberite vašu karticu",    
            },
            
            brojKartice: {
                required: "Broj kartice je potrebno polje",
                minlength: "Molimo unesite minimalno 4 znaka",
                maxlength: "Molimo unesite točno 4 znaka",
                digits: "Molimo upište broj kartice"
            },
            brojkarticePoslovni: {
                required: "Broj kartice je potrebno polje",
                minlength: "Molimo unesite minimalno 4 znaka",
                maxlength: "Molimo unesite točno 4 znaka",
                digits: "Molimo upište broj kartice"
            },
            ime: "Ime je potrebno polje",
            imePoslovni: "Ime je potrebno polje",
            prezime: "Prezime je potrebno polje",
            prezimePoslovni: "Prezime je potrebno polje",
            email: {
                required: "E-mail je potrebno polje",
                email: "Molimo unesite važeći email",
            },
            emailPoslovni: {
                required: "Email je potrebno polje",
                email: "Molimo unesite važeći email",
            },

            razlogOdgode: {
                required: "Molimo odaberite opciju"
            },
            razlogOdgodePoslovni: {
                required: "Molimo odaberite opciju"
            },
            predmetOdgode:{
                required: "Molimo odaberite opciju"
            },
            predmetOdgode2:{
                required: "Molimo odaberite opciju"
            },
            zatrazenaOdgoda: {
                required: "Molimo odaberite opciju"
            },
            zatrazenaOdgoda2: {
                required: "Molimo odaberite opciju"
            },
            zatrazenaOdgodaPoslovni: {
                required: "Molimo odaberite opciju"
            },

            zatrazenaOdgodaPoslovni2: {
                required: "Molimo odaberite opciju"
            },

            zatrazenaOdgodaPoslovni2: {
                required: "Molimo dodajte privitak"
            },
            prednjaStrana: {
                required: "Molimo dodajte privitak"
            },
            straznjaStrana: {
                required: "Molimo dodajte privitak"
            },
            dokumentacija: {
                required: "Molimo dodajte privitak"
            },
            prednjaStranaPoslovni: {
                required: "Molimo dodajte privitak"
            },
            straznjaStranaPoslovni: {
                required: "Molimo dodajte privitak"
            },
            dokumentacijaPoslovni: {
                required: "Molimo dodajte privitak"
            },


            suglasnost1: {
                required: "Molimo potvrdite opciju"
            },
            suglasnost2: {
                required: "Molimo potvrdite opciju"
            },
            suglasnostPoslovni1: {
                required: "Molimo potvrdite opciju"
            },
            suglasnostPoslovni2: {
                required: "Molimo potvrdite opciju"
            },
            ostalo: {
                required: "Ostalo je potrebno polje",
                minlength: "Polje ostalo bi trebalo imati minimalno 8 znakova",
                maxlength: "Polje ostalo ne bi trebalo imati više od 500 znakova",
            },
            ostaloPoslovni: {
                required: "Ostalo je potrebno polje",
                minlength: "Polje ostalo bi trebalo imati minimalno 8 znakova",
                maxlength: "Polje ostalo ne bi trebalo imati više od 500 znakova",
            },
        }
    }


    $("#myForm").multiStepForm(

        {
            // defaultStep:0,
            callback: function () {

                console.log("save");
            },
            beforeSubmit: function (form, submit) {

                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,

        }
    ).navigateTo(0);

    $("#myForm2").multiStepForm(

        {
            // defaultStep:0,

            callback: function () {
                console.log("save");
            },
            beforeSubmit: function (form, submit) {

                console.log("called before submiting the form");
                console.log(form);
                console.log(submit);
            },
            validations: val,
        }
    ).navigateTo(0);
});